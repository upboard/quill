FROM node:20 as builder

# Copy source code and navigate to it.
COPY . /quill
WORKDIR /quill

# Authenticate with Google Artifact Registry.
RUN cp .npmrc /root/.npmrc
RUN npx google-artifactregistry-auth

# Publish the module to Google Artifact Registry.
RUN npm publish
