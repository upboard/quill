import Parchment from '@praxie/parchment';

const config = {
    scope: Parchment.Scope.BLOCK
};

class ListStyleAttributor extends Parchment.Attributor.Class {
    add(node, value) {
        if (!this.canAdd(node, value)) {
            return false;
        }
        if (value && typeof value === 'boolean') {
            this.remove(node);
            node.classList.add(this.keyName + '-applied');
        } else {
            this.remove(node);
        }
        return true;
    }

    canAdd(node, value) {
        return node.tagName === 'LI' && super.canAdd(node, value);
    }

    remove(node) {
        node.classList.remove(this.keyName + '-applied');
    }

    value(node) {
        return node.classList.contains(this.keyName + '-applied');
    }
}

const ListBoldClass = new ListStyleAttributor('listbold', 'ql-listbold', config);
const ListItalicClass = new ListStyleAttributor('listitalic', 'ql-listitalic', config);

export { ListBoldClass, ListItalicClass };
