import Inline from '../blots/inline';


class Link extends Inline {
  static create(value) {
    let link = typeof value === 'string' ? value : value.link || '';
    let href = typeof value === 'string' ? value : value && value.href || '';
    let node = super.create(link);
    node.setAttribute('href', href);
    node.setAttribute('smartLinkId', link);
    node.setAttribute('rel', 'noopener noreferrer');
    node.setAttribute('target', '_blank');
    return node;
  }

  static formats(domNode) {
    return {
      link: domNode.getAttribute('smartLinkId'),
      href: domNode.getAttribute('href')
    };
  }

  static sanitize(url) {
    return sanitize(url, this.PROTOCOL_WHITELIST) ? url : this.SANITIZED_URL;
  }

  format(name, value) {
    let link = typeof value === 'string' ? value : value && value.link || '';
    let href = typeof value === 'string' ? value : value && value.href || '';
    if (name !== this.statics.blotName || !value || !link) return super.format(name, link);
    this.domNode.setAttribute('href', href);
    this.domNode.setAttribute('smartLinkId', link);
  }
}
Link.blotName = 'link';
Link.tagName = 'A';
Link.SANITIZED_URL = 'about:blank';
Link.PROTOCOL_WHITELIST = ['http', 'https', 'mailto', 'tel'];


function sanitize(url, protocols) {
  let anchor = document.createElement('a');
  anchor.href = url;
  let protocol = anchor.href.slice(0, anchor.href.indexOf(':'));
  return protocols.indexOf(protocol) > -1;
}


export { Link as default, sanitize };
