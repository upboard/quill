import Parchment from '@praxie/parchment';

let SizeClass = new Parchment.Attributor.Class('size', 'ql-size', {
  scope: Parchment.Scope.INLINE,
  whitelist: ['small', 'large', 'huge']
});
var sizeWhiltelist = [...new Array(99)].map((e, index) => `${index+1}px`);
let SizeStyle = new Parchment.Attributor.Style('size', 'font-size', {
  scope: Parchment.Scope.INLINE,
  whitelist: sizeWhiltelist
});

export { SizeClass, SizeStyle };
